Question: How do you build Pandoc such that you can build libraries against it?

# Attempt 1: Sandbox, but install to prefix. Doesn't work.

```
mkdir pandoc_build
cd pandoc_build/
cabal sandbox init
cabal update # creates ~/.cabal
cabal install --only-dependencies pandoc
cabal install --prefix=/usr/local pandoc
```

Problem with this approach is that, while you do get the Pandoc binary
in `/usr/local/bin`, you can't build libraries against it. The
libraries *are* installed in `/usr/local/lib`, but the recursive
dependencies are not. So while you can hack GHC's package database to
insert Pandoc, the resulting build won't work.

```
# WARNING: Doesn't work
# GHC rejects package because recursive dependencies are not present
cabal sandbox hc-pkg describe pandoc > pandoc.pkg
ghc-pkg register --force pandoc.pkg
```

# Attempt 2: Non-sandbox install to prefix. Blow away ~/.cabal and see if it still works.

```
cabal update
cabal install --prefix=/usr/local pandoc
# Optional: rm -rf ~/.cabal
```

Works, but still fairly large build output.

# Attempt 3: Use Ubuntu packages

```
apt install pandoc libghc-pandoc-types-dev
```

Note: You can avoid installing the `libghc-pandoc-dev` package because
concat only depends on pandoc-types.

Note: This depends on Ubuntu having a sufficiently up-to-date package
version, which as of Pandoc 2.0 isn't likely to be true until Ubuntu
18.04.

# How to build Docker image

```
cd docker
./build.sh
```
