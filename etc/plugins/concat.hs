---- Copyright (c) 2013-2023, Elliott Slaughter <elliottslaughter@gmail.com>
----
---- Permission is hereby granted, free of charge, to any person
---- obtaining a copy of this software and associated documentation
---- files (the "Software"), to deal in the Software without
---- restriction, including without limitation the rights to use, copy,
---- modify, merge, publish, distribute, sublicense, and/or sell copies
---- of the Software, and to permit persons to whom the Software is
---- furnished to do so, subject to the following conditions:
----
---- The above copyright notice and this permission notice shall be
---- included in all copies or substantial portions of the Software.
----
---- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
---- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
---- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
---- NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
---- HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
---- WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
---- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
---- DEALINGS IN THE SOFTWARE.
----

import Data.Aeson
import qualified Data.ByteString.Lazy as B
import Data.Maybe
import System.Environment
import Text.Pandoc.Definition

lowerHeaders :: Block -> Block
lowerHeaders (Header level attr xs) | level == 6 = Header level attr xs
lowerHeaders (Header level attr xs) | level < 6 = Header (level + 1) attr xs
lowerHeaders x = x

emptyDoc :: Pandoc
emptyDoc = Pandoc nullMeta []

concatDocs :: [Pandoc] -> Pandoc
concatDocs documents =
  case documents of
    (first : rest) -> foldl mergeDocs first rest
    _ -> emptyDoc
  where mergeDocs (Pandoc metaA bodyA) (Pandoc metaB bodyB) =
          Pandoc metaA (bodyA ++ (let title = docTitle metaB in if title /= [] then [Header 1 nullAttr title] else []) ++ map Para (filter (\x -> x /= []) (docAuthors metaB)) ++ map lowerHeaders bodyB)

concatMarkdown :: [B.ByteString] -> B.ByteString
concatMarkdown = encode . concatDocs . (map (fromJust . decode))

interactWithFiles :: ([B.ByteString] -> B.ByteString) -> FilePath -> [FilePath] -> IO ()
interactWithFiles converter outFilename inFilenames =
  mapM B.readFile inFilenames >>= (B.writeFile outFilename) . converter

interactWithArgs :: ([B.ByteString] -> B.ByteString) -> [String] -> IO ()
interactWithArgs converter args =
  case args of
    (outFilename : inFilenames) -> interactWithFiles converter outFilename inFilenames
    _ -> usage

usage :: IO ()
usage = fail "Usage: ./cat <output.native> <input.md>*"

main = getArgs >>= interactWithArgs concatMarkdown
