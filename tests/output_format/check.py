#!/usr/bin/env python3
#### Copyright (c) 2013-2023, Elliott Slaughter <elliottslaughter@gmail.com>
####
#### Permission is hereby granted, free of charge, to any person
#### obtaining a copy of this software and associated documentation
#### files (the "Software"), to deal in the Software without
#### restriction, including without limitation the rights to use, copy,
#### modify, merge, publish, distribute, sublicense, and/or sell copies
#### of the Software, and to permit persons to whom the Software is
#### furnished to do so, subject to the following conditions:
####
#### The above copyright notice and this permission notice shall be
#### included in all copies or substantial portions of the Software.
####
#### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#### NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#### HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#### WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#### OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#### DEALINGS IN THE SOFTWARE.
####

import os

chapter1 = "Chapter 1"
chapter2 = "Chapter 2"


def check():
    # Sanity check that all the output files exist.
    assert os.path.exists("build/all.docx")
    assert os.path.exists("build/all.epub")
    assert os.path.exists("build/all.html")
    assert os.path.exists("build/all.pdf")
    assert os.path.exists("build/all.tex")

    assert os.path.exists("build/00.docx")
    assert os.path.exists("build/00.epub")
    assert os.path.exists("build/00.html")
    assert os.path.exists("build/00.pdf")
    assert os.path.exists("build/00.tex")

    assert os.path.exists("build/01.docx")
    assert os.path.exists("build/01.epub")
    assert os.path.exists("build/01.html")
    assert os.path.exists("build/01.pdf")
    assert os.path.exists("build/01.tex")

    # Some more specific checks in the HTML files.
    with open("build/all.html") as f:
        c = f.read()
        assert c.find(chapter1) >= 0
        assert c.find(chapter2) >= 0

    with open("build/00.html") as f:
        c = f.read()
        assert c.find(chapter1) >= 0
        assert c.find(chapter2) < 0

    with open("build/01.html") as f:
        c = f.read()
        assert c.find(chapter1) < 0
        assert c.find(chapter2) >= 0


if __name__ == "__main__":
    check()
